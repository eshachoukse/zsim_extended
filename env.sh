#!/bin/sh
PINPATH=~/Documents/Research/pin-2.14-71313-gcc.4.4.7-linux/
SPECPATH=~/Documents/Research/SPECcpu2006_install/benchspec/CPU2006/
NVMAINPATH=~/Documents/Research/shma/SHMA/zsim-nvmain/nvmain
ZSIMPATH=~/Documents/Research/shma/SHMA/zsim-nvmain
BOOST=/usr/
HDF5=/usr/
SIMPOINT_DIR=~/Documents/Research/SimPoint.3.2/
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$PINPATH/intel64/lib:$PINPATH/intel64/runtime:$PINPATH/intel64/lib:$PINPATH/intel64/lib-ext:$BOOST/lib:$HDF5/lib:/usr/local/gmp-4.3.2/lib:/usr/local/mpfr-2.4.2/lib:/usr/local/mpc-0.8.1/lib
INCLUDE=$INCLUDE:$HDF5/include
LIBRARY_PATH=$LIBRARY_PATH:$HDF5/lib
CPLUS_INCLUDE_PATH=$CPLUS_INCLUDE_PATH:$HDF5/include
export ZSIMPATH PINPATH NVMAINPATH LD_LIBRARY_PATH BOOST CPLUS_INCLUDE_PATH LIBRARY_PATH SIMPOINT_DIR
sysctl -w kernel.shmmax=9073741824
