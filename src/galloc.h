/** $lic$
 * Copyright (C) 2012-2014 by Massachusetts Institute of Technology
 * Copyright (C) 2010-2013 by The Board of Trustees of Stanford University
 *
 * This file is part of zsim.
 *
 * zsim is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, version 2.
 *
 * If you use this software in your research, we request that you reference
 * the zsim paper ("ZSim: Fast and Accurate Microarchitectural Simulation of
 * Thousand-Core Systems", Sanchez and Kozyrakis, ISCA-40, June 2013) as the
 * source of the simulator in any publications that use this software, and that
 * you send us a citation of your work.
 *
 * zsim is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GALLOC_H_
#define GALLOC_H_

#include <memory>
#include <stdlib.h>
#include <string.h>
#include <cstddef>
#include <cstdint>

int gm_init(size_t segmentSize);

void gm_attach(int shmid);

// C-style interface
void* gm_malloc(size_t size);
void* __gm_calloc(size_t num, size_t size);  //deprecated, only used internally
void* __gm_memalign(size_t blocksize, size_t bytes);  // deprecated, only used internally
char* gm_strdup(const char* str);
void gm_free(void* ptr);

// C++-style alloc interface (preferred)
template <typename T> T* gm_malloc() {return static_cast<T*>(gm_malloc(sizeof(T)));}
template <typename T> T* gm_malloc(size_t objs) {return static_cast<T*>(gm_malloc(sizeof(T)*objs));}
template <typename T> T* gm_calloc() {return static_cast<T*>(__gm_calloc(1, sizeof(T)));}
template <typename T> T* gm_calloc(size_t objs) {return static_cast<T*>(__gm_calloc(objs, sizeof(T)));}
template <typename T> T* gm_memalign(size_t blocksize) {return static_cast<T*>(__gm_memalign(blocksize, sizeof(T)));}
template <typename T> T* gm_memalign(size_t blocksize, size_t objs) {return static_cast<T*>(__gm_memalign(blocksize, sizeof(T)*objs));}
template <typename T> T* gm_dup(T* src, size_t objs) {
    T* dst = gm_malloc<T>(objs);
    memcpy(dst, src, sizeof(T)*objs);
    return dst;
}

void gm_set_glob_ptr(void* ptr);
void* gm_get_glob_ptr();

void gm_set_secondary_ptr(void* ptr);
void* gm_get_secondary_ptr();

void gm_stats();

bool gm_isready();
void gm_detach();
template<typename T>
class Allocator {
	public : 
			//    typedefs
		typedef T value_type;
		typedef value_type* pointer;
		typedef const value_type* const_pointer;
		typedef value_type& reference;
		typedef const value_type& const_reference;
		typedef std::size_t size_type;
		typedef std::ptrdiff_t difference_type;

	public : 
		//    convert an allocator<T> to allocator<U>
		template<typename U>
			struct rebind {
			typedef Allocator<U> other;
		};

	public : 
		inline explicit Allocator() {}
		inline ~Allocator() {}
		inline explicit Allocator(Allocator const&) {}
		template<typename U>
		inline explicit Allocator(Allocator<U> const&) {}
		//    address
		inline pointer address(reference r) { return &r; }
		inline const_pointer address(const_reference r) { return &r; }

		//    memory allocation
		inline pointer allocate(size_type cnt, 
					typename std::allocator<void>::const_pointer = 0) { 
			return gm_malloc<T>();
			//reinterpret_cast<pointer>(::operator new(cnt * sizeof (T))); 
		}
		inline void operator delete(void *p, size_t sz) {
            gm_free(p);
        }
		inline void deallocate(pointer p, size_type) { 
			::operator delete(p); 
		}
		inline void deallocate(pointer p) { 
			::operator delete(p); 
		}
		inline void erase(pointer p) { 
			::operator delete(p); 
		}

		//    construction/destruction
		inline void construct(pointer p, const T& t) { new(p) T(t); }
		inline void destroy(pointer p) { p->~T(); }

		inline bool operator==(Allocator const&) { return true; }
		inline bool operator!=(Allocator const& a) { return !operator==(a); }
};    //    end of class Allocator 


class GlobAlloc {
    public:
        virtual ~GlobAlloc() {}

        inline void* operator new (size_t sz) {
            return gm_malloc(sz);
        }
        
	inline void operator delete(void *p, size_t sz) {
            gm_free(p);
        }

        //Placement new
        inline void* operator new (size_t sz, void* ptr) {
            return ptr;
        }
        
		//Placement delete... make ICC happy. This would only fire on an exception
        void operator delete (void* p, void* ptr) {}
};

#endif  // GALLOC_H_
