/*
 * 2015-xxx by YuJie Chen
 * email:    YuJieChen_hust@163.com
 * function: extend zsim-nvmain with some other simulation,such as tlb,page table,page table,etc.  
 */
#ifndef PAGE_TABLE_WALKER_H
#define PAGE_TABLE_WALKER_H
#define PAGEMAP_ENTRY 8
#define GET_BIT(X,Y) (X & ((uint64_t)1<<Y)) >> Y
#define GET_PFN(X) X & 0x7FFFFFFFFFFFFF
#define MAXLINE 300
//#define CHUNK_512B

const int __endian_bit = 1;
#define is_bigendian() ( (*(char*)&__endian_bit) == 0 )

#include <map>
#include <fstream>
#include "locks.h"
#include "g_std/g_string.h"
#include "memory_hierarchy.h"
#include "page-table/comm_page_table_op.h"
#include "common/global_const.h"
#include "page-table/page_table.h"

#include "MMU/memory_management.h"
#include "tlb/common_func.h"
#include "tlb/hot_monitor_tlb.h"
#include "DRAM-buffer/DRAM_buffer_block.h"
#include "tlb/tlb_entry.h"
#include "tlb/fullycache.h"

#include "src/nvmain_mem_ctrl.h"
#include "include/NVMainRequest.h"
#include "core.h"
#include "ooo_core.h"
#include "BPCompressor.hh"
#include "ipt.h"

#ifdef CHUNK_512B 
template <class T>
class PageTableWalker: public BasePageTableWalker
{
	public:
		//access memory
		PageTableWalker(const g_string& name , PagingStyle style): pg_walker_name(name),procIdx((uint32_t)(-1))
	{
		mode = style;
		period = 0;
		dirty_evict = 0;
		total_evict = 0;
		allocated_page = 0;
		for (int i=0;i<9;i++ ){
			Allo_Page[i]=0;
			Init_Page[i] = 0;
		}
		metadata_accesses=metadata_misses=0;
		LRUFullycache* tempcache0 = gm_memalign<LRUFullycache>(CACHE_LINE_BYTES, 1); 
		LRUFullycache* tempcache1 = gm_memalign<LRUFullycache>(CACHE_LINE_BYTES, 1); 
		metacache = new (tempcache0) LRUFullycache(512); 
		pagemovecache = new (tempcache1) LRUFullycache(64); 
		pages_moved=line_movement=0;
		mmap_cached = 0;
		tlb_shootdown_overhead = 0;
		hscc_tlb_shootdown = 0;
		pcm_map_overhead = 0;
		hscc_map_overhead = 0;
		tlb_miss_exclude_shootdown = 0;
		tlb_miss_overhead = 0;
		futex_init(&walker_lock);
		BPSCompressor64* tempcomp =  gm_memalign<BPSCompressor64>(CACHE_LINE_BYTES, 1); 
		mycomp = new (tempcomp) BPSCompressor64("BPC64_5", 2, 4, 10, 2);   // ESHA GOLDEN // 64B, 8bins
		//mycomp = new BPSCompressor64("BPC64_5", 2, 4, 10, 2);   // ESHA GOLDEN // 64B, 8bins
		f = fopen("/proc/self/pagemap", "rb");
		fflags = fopen("/proc/kpageflags", "rb");
		info("zinfo->is_real_proce_based_paging : %d \n", zinfo->is_real_proce_based_paging);
		if(!f)
			info("ERROR :: Procfile pagemap could not be opened") ;
	}
		~PageTableWalker(){}
		/*------tlb hierarchy related-------*/
		//bool add_child(const char* child_name ,  BaseTlb* tlb);
		/*------simulation timing and state related----*/
		uint64_t access( MemReq& req)
		{

			assert(paging);
			period++;
			Address addr = PAGE_FAULT_SIG;
			Address init_cycle = req.cycle;
			if(!zinfo->is_real_proce_based_paging) {

				addr = paging->access(req);

				tlb_miss_exclude_shootdown += (req.cycle - init_cycle);
				//page fault
				if( addr == PAGE_FAULT_SIG )	
				{
					addr = do_page_fault(req , PCM_PAGE_FAULT);
				}
				tlb_miss_overhead += (req.cycle - init_cycle);
			}
			else {
				addr = lookup_procfile(req); //This is the VPN
				if(zinfo->is_main_mem_compressed) {
					Address FullOSPA = (addr << zinfo->page_shift) | (req.lineAddr & (zinfo->lineSize - 1));
					req.setOSPA(FullOSPA);
					//VA is the line addr in VA
					UINT64 pageAddr = (req.lineAddr >> zinfo->page_shift ) << zinfo->page_shift;

					//Now, we have the OSPA. Based on this, look up the <VA, PA> pair and match
					Address OSPAPFN;
					uint64_t shifter = ((uint64_t)req.srcId) << 50;
					int OSPAMatch = OSPA_to_PA_Table.OSPA_mapped(getOSPA(req.srcId,addr), shifter|pageAddr);
				//	info("TLB looked up : %llx found %d ", addr, OSPAMatch);
					if(OSPAMatch == 1) {
						//Nothing to do here, unless it is an LLC eviction -- 
						//then we will do it in mem
						//info("OSPA matched\n");
						req.cycle+=(zinfo->mem_access_time*2);
					}	
					else if (OSPAMatch == -1) {
						//ESHA : OSPA not found at all, allocate a page
						//Note that this is paging in the MPA space
						//info("OSPA not found\n");
						do_page_fault(req, PCM_PAGE_FAULT);
					}
					else if (OSPAMatch == -2) {
						//info("OSPA-VA not found\n");
						//DOING THIS HERE BECAUSE IN SIMULATOR, WE DONT SEE THE OS PAGE MOVES
						IPTEntry* PA_to_free = OSPA_to_PA_Table.getIPT(getOSPA(req.srcId,addr));
						OSPA_to_PA_Table.OSPA_erase(getOSPA(req.srcId, addr));
						if(PA_to_free->order == 0) {
							zinfo->buddy_allocator->free_one_page(PA_to_free->PA, PA_to_free->order);
						}
						else {
							zinfo->buddy_allocator->free_one_page(PA_to_free->PA, PA_to_free->order-1);
						}
						Allo_Page[PA_to_free->order]--;
						do_page_fault(req, PCM_PAGE_FAULT);
						//ESHA : Ask Mattan if this should be done or not
						//OSPA found, but VA not matched, free this page, 
						//allocate a new page of required size
					}
					else
						assert(0);
				}
				else{
					req.cycle+=(zinfo->mem_access_time*2);
				}
				tlb_miss_overhead += (req.cycle - init_cycle);
			}
			//suppose page table walking time when tlb miss is 20 cycles
			return addr;	//find address
		}

		void write_through( MemReq& req)
		{
			assert(paging);
			paging->access(req);
		}

		BasePaging* GetPaging()
		{ return paging;}
		void SetPaging( uint32_t proc_id , BasePaging* copied_paging)
		{
			futex_lock(&walker_lock);
			procIdx = proc_id;
			paging = copied_paging;
			futex_unlock(&walker_lock);
		}

		void convert_to_dirty( Address block_id)
		{
			zinfo->dram_manager->convert_to_dirty( procIdx , block_id );
		}
		const char* getName()
		{  return pg_walker_name.c_str(); }

		void calculate_stats()
		{
			//for(auto it=Moved_Pages.begin(); it!=Moved_Pages.end(); it++){
			//	info("%llx %d", it->first, it->second);
			//}
			info("%s evict time:%lu \t dirty evict time: %lu \n",getName(),total_evict,dirty_evict);
			info("%s allocated pages:%lu \n", getName(),allocated_page);
			info("%s TLB shootdown overhead:%llu \n", getName(), tlb_shootdown_overhead);
			info("%s HSCC TLB shootdown overhead:%llu \n", getName(), hscc_tlb_shootdown);
			info("%s PCM page mapping overhead:%llu \n", getName(), pcm_map_overhead);
			info("%s DRAM page mapping overhead:%llu \n", getName(), hscc_map_overhead);
			info("%s TLB miss overhead(exclude TLB shootdown and page fault): %llu", getName(),tlb_miss_exclude_shootdown);
			info("%s TLB miss overhead (include TLB shootdown and page fault): %llu",getName(), tlb_miss_overhead);
			info("%s METADATA Accesses: %llu   Misses: %llu",getName(), metadata_accesses, metadata_misses);
			info("%s Line Movement Read/Wr Requests: %llu ",getName(), line_movement);
			info("%s Pages Moved: %llu Page Movement Read/Wr Requests: %llu UniquePages: %d",getName(), pages_moved, pages_moved*128, Moved_Pages.size());
			
			total_pages=0;
			compressed=0;	
			info("In the beginning, pages are :  ") ;
			for (int i=0; i<9; i++){
				total_pages+= Init_Page[i]*4096*8;
				compressed += (Init_Page[i])*(page_sizes[i]);
				info("%d", Init_Page[i]);	
			}
			info("Compression: %llu compressed: %llu %.6f", total_pages, compressed, (float)(total_pages)/(float)(compressed));

			total_pages=0;
			compressed=0;	
			info("In the end, pages are :  ") ;
			for (int i=0; i<9; i++){
				total_pages+= Allo_Page[i]*4096*8;
				compressed += (Allo_Page[i])*(page_sizes[i]);
				info("%d", Allo_Page[i]);	
				Allo_Page[i]=0;
			}
			info("Compression: %llu compressed: %llu %.6f", total_pages, compressed, (float)(total_pages)/(float)(compressed));
			
			int order = 0;
			for(auto it = VAtoOSPA.begin(); it!=VAtoOSPA.end(); it++){	
				uint64_t pageAddr = (it->first) << zinfo->page_shift;
				order = mycomp->find_page_size_needed(pageAddr, zinfo->cacheline_bins_type);
				Allo_Page[order]++;
			}
			total_pages=0;
			compressed=0;	
			info("MAX In the end, pages are :  ") ;
			for (int i=0; i<9; i++){
				total_pages+= Allo_Page[i]*4096*8;
				compressed += (Allo_Page[i])*(page_sizes[i]);
				info("%d", Allo_Page[i]);	
				Allo_Page[i]=0;
			}
			info("Compression: %llu compressed: %llu %.6f", total_pages, compressed, (float)(total_pages)/(float)(compressed));

		}

		Address lookup_procfile(MemReq& req)
		{
			futex_lock(&walker_lock);
			//TLB shootdown
			Address vpn = req.lineAddr>>(zinfo->page_shift);
			BaseTlb* tmp_tlb = NULL;
			T* entry = NULL;
			for( uint64_t i = 0; i<zinfo->numCores; i++)
			{
				tmp_tlb = zinfo->cores[i]->getInsTlb();
				union
				{	
					CommonTlb<T>* com_tlb;
					HotMonitorTlb<T>* hot_tlb;
				};
				if( zinfo->tlb_type == COMMONTLB )
				{
					com_tlb = dynamic_cast<CommonTlb<T>* >(tmp_tlb); 
					entry = com_tlb->look_up(vpn);
				}
				//instruction TLB IPI
				if( entry )
				{
					entry->set_invalid();
					tlb_shootdown_overhead += zinfo-> tlb_hit_lat; 
					req.cycle += zinfo->tlb_hit_lat;
					entry = NULL;
				}
				tmp_tlb = zinfo->cores[i]->getDataTlb();
				if( zinfo->tlb_type == COMMONTLB)
				{
					com_tlb = dynamic_cast<CommonTlb<T>* >(tmp_tlb); 
					entry = com_tlb->look_up(vpn);
				}
				if(entry )
				{
					entry->set_invalid();
					tlb_shootdown_overhead += zinfo-> tlb_hit_lat; 
					req.cycle += zinfo->tlb_hit_lat;
					entry = NULL;
				}
			}
			//*********TLB shoot down ended
			int stall =0;
			uint64_t read_val = 0;
			uint64_t nread_val = 0;
			//info ("Src ID was %d", req.srcId);
			uint64_t shifter = ((uint64_t)req.srcId) << 50;
			if (zinfo->numCores == 1 ){	
				while(1) {
					uint64_t file_offset = vpn * PAGEMAP_ENTRY;
					int status = fseek(f, file_offset, SEEK_SET);
					if(status){
						info("ERROR :: Failed to do fseek! address: 0x%lx", vpn);
						//sleep(1);
						//return -1;

					}
					else {
						errno = 0;
						unsigned char c_buf[PAGEMAP_ENTRY];
						for(int i=0; i < PAGEMAP_ENTRY; i++){
							int c = getc(f);
							if(c==EOF){
								info("ERROR :: Reached end of the file\n");
								//VAtoOSPA[vpn] = (Address) (req.srcId << 20) | vpn;
								VAtoOSPA[ shifter|vpn] =  shifter|vpn; 
								futex_unlock(&walker_lock);
								return vpn;
							}
							if(is_bigendian())
								c_buf[i] = c;
							else
								c_buf[PAGEMAP_ENTRY - i - 1] = c;
						}
						for(int i=0; i < PAGEMAP_ENTRY; i++){
							read_val = (read_val << 8) + c_buf[i];
						}
						//printf("\n");
						if(GET_BIT(read_val, 63)){
							//info("PFN: 0x%llx vpn : 0x%lx",(unsigned long long) GET_PFN(read_val), vpn);
							//Now read the kpageflags
							uint64_t nfile_offset = ((unsigned long long)GET_PFN(read_val) )* PAGEMAP_ENTRY;
							int nstatus = fseek(fflags, nfile_offset, SEEK_SET);
							if(nstatus){
								info("ERROR :: Failed to do fseek! address: 0x%lx", vpn);
							}
							else {
								unsigned char nc_buf[PAGEMAP_ENTRY];
								for(int i=0; i < PAGEMAP_ENTRY; i++){
									int nc = getc(f);
									if(nc==EOF){
										info("ERROR :: Reached end of the file\n");
										//VAtoOSPA[vpn] = (req.srcId << 20) | vpn; 
										VAtoOSPA[ shifter |vpn] =  shifter |vpn; 
										//VAtoOSPA[vpn] = vpn;
										futex_unlock(&walker_lock);
										return vpn;
									}
									if(is_bigendian())
										nc_buf[i] = nc;
									else
										nc_buf[PAGEMAP_ENTRY - i - 1] = nc;
								}
								for(int i=0; i < PAGEMAP_ENTRY; i++){
									nread_val = (nread_val << 8) + nc_buf[i];
								}
								//							if(GET_BIT(nread_val, 22) || GET_BIT(nread_val, 17)){ //THP bit in kpageflags
								//								//info("THP");
								//							}
							}
							//ESHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA		
							//VAtoOSPA[vpn] = (Address) GET_PFN(read_val);
							//VAtoOSPA[vpn] = (req.srcId << 20) | vpn; 
							VAtoOSPA[ shifter|vpn] =  shifter |vpn; 
							futex_unlock(&walker_lock);
							return vpn;
						}
					}
					//ESHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAaa
					info("Stalling at 0x%lx", vpn);
					//if(stall++ == 1) break;			  
					//sleep(1);
					break;
				}
			}
			//VAtoOSPA[vpn] = (Address) vpn;
			VAtoOSPA[ shifter|vpn] =  shifter |vpn; 
			futex_unlock(&walker_lock);
			return shifter|vpn;
		}

		Address do_page_fault(MemReq& req, PAGE_FAULT fault_type)
		{
			//allocate one page from Zone_Normal area
			//ESHA If compression is ON, then the page's size is determined
			// based on the compressed size of the page
			futex_lock(&walker_lock);
			debug_printf("page fault, allocate free page through buddy allocator");
			UINT64 pageAddr = (req.lineAddr >> zinfo->page_shift ) << zinfo->page_shift;
			Page* page = NULL;
			unsigned order = 0;
			if( zinfo->is_main_mem_compressed ) {
				order = mycomp->find_page_size_needed(pageAddr, zinfo->cacheline_bins_type);
			}
			if( zinfo->buddy_allocator)
			{
				if(order == 0){
					page = zinfo->buddy_allocator->allocate_pages(0, order);
				}
				else{
					page = zinfo->buddy_allocator->allocate_pages(0, order-1);
				}
				Allo_Page[order]++;
				Init_Page[order]++;
				if(page)
				{
					//TLB shootdown
					Address vpn = req.lineAddr>>(zinfo->page_shift);
					BaseTlb* tmp_tlb = NULL;
					T* entry = NULL;
					for( uint64_t i = 0; i<zinfo->numCores; i++)
					{
						tmp_tlb = zinfo->cores[i]->getInsTlb();
						union
						{	
							CommonTlb<T>* com_tlb;
							HotMonitorTlb<T>* hot_tlb;
						};
						if( zinfo->tlb_type == COMMONTLB )
						{
							com_tlb = dynamic_cast<CommonTlb<T>* >(tmp_tlb); 
							entry = com_tlb->look_up(vpn);
						}
						else if( zinfo->tlb_type == HOTTLB)
						{
							hot_tlb = dynamic_cast<HotMonitorTlb<T>* >(tmp_tlb); 
							entry = hot_tlb->look_up(vpn);
						}
						//instruction TLB IPI
						if( entry )
						{
							entry->set_invalid();
							tlb_shootdown_overhead += zinfo-> tlb_hit_lat; 
							req.cycle += zinfo->tlb_hit_lat;
							entry = NULL;
						}
						tmp_tlb = zinfo->cores[i]->getDataTlb();
						if( zinfo->tlb_type == COMMONTLB)
						{
							com_tlb = dynamic_cast<CommonTlb<T>* >(tmp_tlb); 
							entry = com_tlb->look_up(vpn);
						}
						else if( zinfo->tlb_type == HOTTLB)
						{
							hot_tlb = dynamic_cast<HotMonitorTlb<T>* >(tmp_tlb); 
							entry = hot_tlb->look_up(vpn);
						}
						if(entry)
						{
							entry->set_invalid();
							tlb_shootdown_overhead += zinfo-> tlb_hit_lat; 
							req.cycle += zinfo->tlb_hit_lat;
							entry = NULL;
						}
					}
					//*********TLB shoot down ended
					if( zinfo->enable_shared_memory)
					{
						if( !map_shared_region(req, page) )
						{
							Address overhead = paging->map_page_table( req.lineAddr,(void*)page);
							pcm_map_overhead += overhead;
							req.cycle += overhead;
						}
					}
					else
					{
						Address overhead = paging->map_page_table( req.lineAddr,(void*)page);
						pcm_map_overhead += overhead;
						req.cycle += overhead;
					}
					allocated_page++;
					//cout << "Allocated page VA: " << hex << vpn << dec << " order " << order << endl ;
					CACHELINE_OFFSET_DATA lineOffsets = mycomp->find_cacheline_offsets(pageAddr, zinfo->cacheline_bins_type);
				
					uint64_t shifter = ((uint64_t)req.srcId) << 50;
					OSPA_to_PA_Table.insert(getOSPA(req.srcId,vpn), shifter|pageAddr, page->pageNo, lineOffsets, order, zinfo->cacheline_bins_type);	
					futex_unlock(&walker_lock);
					return getOSPA(req.srcId, req.lineAddr >> zinfo->page_shift);
				}
			}
			//update page table
			futex_unlock(&walker_lock);
			return (req.lineAddr>>zinfo->page_shift);
		}


		bool inline map_shared_region( MemReq& req , void* page)
		{
			Address vaddr = req.lineAddr;
			//std::cout<<"find out shared region"<<std::endl;
			if( !zinfo->shared_region[procIdx].empty())
			{
				int vm_size = zinfo->shared_region[procIdx].size();
				//std::cout<<"mmap_cached:"<<std::dec<<mmap_cached
				//	<<" vm size:"<<std::dec<<vm_size<<std::endl;
				Address vm_start = zinfo->shared_region[procIdx][mmap_cached].start;
				Address vm_end = zinfo->shared_region[procIdx][mmap_cached].end;
				Address vpn = vaddr>>(zinfo->page_shift);
				//belong to the shared memory region
				//examine whether in mmap_cached region (examine mmap_cached firstly)
				if( find_shared_vm(vaddr,
							zinfo->shared_region[procIdx][mmap_cached]) )
				{
					Address overhead = map_all_shared_memory( vaddr, (void*)page);
					req.cycle += overhead;
					pcm_map_overhead += overhead;
					return true;
				}
				//after mmap_cached
				else if( vpn > vm_end && mmap_cached < vm_size-1 )
				{
					mmap_cached++;
					for( ;mmap_cached < vm_size; mmap_cached++)
					{
						if( find_shared_vm(vaddr, 
									zinfo->shared_region[procIdx][mmap_cached]))
						{
							Address overhead = map_all_shared_memory(vaddr, (void*)page);
							req.cycle += overhead;
							pcm_map_overhead += overhead;
							return true;
						}
					}
					mmap_cached--;
				}
				//before mmap_cached
				else if( vpn < vm_start && mmap_cached > 0 )
				{
					mmap_cached--;
					for( ;mmap_cached >= 0; mmap_cached--)
					{
						if( find_shared_vm(vaddr, 
									zinfo->shared_region[procIdx][mmap_cached]))
						{
							Address overhead = map_all_shared_memory(vaddr, (void*)page);
							req.cycle += overhead;
							pcm_map_overhead += overhead;
							return true;
						}
					}
					mmap_cached++;
				}
			}
			return false;
		}

		bool inline find_shared_vm(Address vaddr, Section vm_sec)
		{
			Address vpn = vaddr >>(zinfo->page_shift);
			if( vpn >= vm_sec.start && vpn < vm_sec.end )
				return true;
			return false;
		}

		int inline map_all_shared_memory( Address va, void* page)
		{
			int latency = 0;
			for( uint32_t i=0; i<zinfo->numProcs; i++)
			{
				assert(zinfo->paging_array[i]);
				latency += zinfo->paging_array[i]->map_page_table(va,page);
			}
			return latency;
		}

		DRAMBufferBlock* allocate_page( )
		{
			if( zinfo->dram_manager->should_reclaim() )
			{
				zinfo->dram_manager->evict( zinfo->dram_evict_policy);
			}
			//std::cout<<"allocate page table"<<std::endl;
			return (zinfo->dram_manager)->allocate_one_page( procIdx);
		}

		void reset_tlb( T* tlb_entry)
		{
			tlb_entry->set_in_dram(false);
			tlb_entry->clear_counter();
		}

		bool update_metacache(MemReq& req) {
			uint64_t OSPAPFN = getOSPA(req.cid, req.lineAddr>>(zinfo->page_shift - lineBits));
			//info("Metadata : %lx " ,OSPAPFN );
			metadata_accesses++;
			if(metacache->access(OSPAPFN)) {
				return 1;
			} 
			return 0;
		}

		uint64_t metadata(MemReq& req) {	
			//The metadata prefetch is currently disabled
			//Seems to reduce the hit rate -- quite bad for non-spatially-local benches
			//metacache->access(OSPAPFN | 0x1); //One cache line has two entries
			//metacache->access(OSPAPFN & (-2)); //One cache line has two entries
			uint64_t OSPAPFN = getOSPA(req.cid, req.lineAddr>>(zinfo->page_shift - lineBits));
			IPTEntry *my_ipt = OSPA_to_PA_Table.getIPT(OSPAPFN);
			metadata_misses++;
			MemReq readReq;
			readReq.set(MemReq::COMPRESSED_PAGE_MOVE);
			readReq.srcId = req.srcId;
			readReq.cycle = req.cycle;
			//readReq.type = GETS;
			readReq.type = METADATA;
			readReq.lineAddr = (my_ipt->PA << 5); //Since each metadata entry is 32bytes
			return zinfo->memoryControllers[0]->COMPaccess(readReq);
		}

		void do_compress_size_check( MemReq& req) 
		{
			futex_lock(&walker_lock);
			//This re.lineAddr is OSPA line addr
			//Since this is not called before the l1 filter cache, lineAddr is cache line addr by this time
			//Now, we have the OSPA. Based on this, look up the <VA, PA> pair and match
			//info("Do_compress_Check");
			CACHELINE_DATA _data;
			uint64_t line_addr = 0;
			bool pagemove=0;
			uint64_t OSPAPFN = getOSPA(req.cid, req.lineAddr>>(zinfo->page_shift - lineBits));
			IPTEntry *my_ipt = OSPA_to_PA_Table.getIPT(OSPAPFN);
			PIN_SafeCopy(&_data, (VOID *) (req.lineAddr << lineBits), zinfo->lineSize); 

			unsigned newSize = linebin(mycomp->compressLine(&_data, line_addr), zinfo->cacheline_bins_type);
			unsigned cacheLineNo = (req.lineAddr & (_LINES_PER_PAGE-1)) ;

			Page* page = NULL;

			if ( newSize != my_ipt->line_offsets.byte[cacheLineNo]) {
				if(newSize < my_ipt->line_offsets.byte[cacheLineNo]) {
					//Size has reduced
					int increasingPage = metacache->line_decrease(OSPAPFN);
					int increasingPage2 = pagemovecache->line_decrease(OSPAPFN);
					info("Size reduced  0x%lx %d from %d to %d\n", req.lineAddr >> 6, my_ipt->VA, my_ipt->line_offsets.byte[cacheLineNo], newSize);
					my_ipt->free_bits += (block_sizes[zinfo->cacheline_bins_type][my_ipt->line_offsets.byte[cacheLineNo]] - block_sizes[zinfo->cacheline_bins_type][newSize]); 

					my_ipt->line_offsets.byte[cacheLineNo] = newSize;	
					if((my_ipt->order != 0) && (increasingPage == 0) && (increasingPage2==-1) && (my_ipt->free_bits >= (page_sizes[my_ipt->order] - page_sizes[my_ipt->order-1] ))) {
						MemReq readReq, writeReq;
						readReq.set(MemReq::COMPRESSED_PAGE_MOVE);
						writeReq.set(MemReq::COMPRESSED_PAGE_MOVE);
						readReq.srcId = writeReq.srcId = req.srcId;
						readReq.cycle = writeReq.cycle = req.cycle;
						readReq.type = GETS;
						writeReq.type = PUTX;

						Address old_ppn = my_ipt->PA;
						if(Moved_Pages.find(my_ipt->VA)!=Moved_Pages.end()){
							Moved_Pages[my_ipt->VA]++;
						}
						else {
							Moved_Pages[my_ipt->VA]=1;
						}
						info("Deallocating page 0x%lx %d for decreased size", my_ipt->VA, my_ipt->PA);
						if(my_ipt->order!=1){
							//All the -1 , -2 is because order = 0 corresponds to a zero page, but in the MPA, 
							//we still have an order 0 page allocated already
							zinfo->buddy_allocator->free_one_page(my_ipt->PA, my_ipt->order - 1);
							page = zinfo->buddy_allocator->allocate_pages(0, my_ipt->order - 2);
							assert(page);
							my_ipt->PA = page->pageNo;
						}
						Allo_Page[my_ipt->order]--;
						Allo_Page[my_ipt->order-1]++;
						Address new_ppn = my_ipt->PA;
						my_ipt->free_bits -= (page_sizes[my_ipt->order] - page_sizes[my_ipt->order-1]);
						my_ipt->order--;
						my_ipt->exceptions = 0;
						info("Deallocated a page for decreased size, 0x%lx to 0x%lx", old_ppn<<zinfo->page_shift, new_ppn<<zinfo->page_shift);
						pages_moved++;
						if(my_ipt->order!=0){
							for(int lineNo = 0; lineNo < _LINES_PER_PAGE; lineNo++) {
								readReq.lineAddr = (old_ppn << zinfo->page_shift) | (lineNo << lineBits);
								writeReq.lineAddr = (new_ppn << zinfo->page_shift) | (lineNo << lineBits);
								writeReq.cycle = zinfo->memoryControllers[0]->COMPaccess(readReq);
								if(lineNo == _LINES_PER_PAGE-1){
									writeReq.type = COMPRESS;
								}
								zinfo->memoryControllers[0]->COMPaccess(writeReq);
							}
						}
						metacache->reset(OSPAPFN, 0);
					}
				}
				else {
					if(my_ipt->order==8) {
						my_ipt->free_bits -= (block_sizes[zinfo->cacheline_bins_type][newSize] - block_sizes[zinfo->cacheline_bins_type][my_ipt->line_offsets.byte[cacheLineNo]]); 
						my_ipt->line_offsets.byte[cacheLineNo] = newSize;	
					}
					else {
						//Size has increased
						info("Size increased 0x%lx %lx %d from %d to %d\n", req.lineAddr >> 6, OSPAPFN, my_ipt->PA, my_ipt->line_offsets.byte[cacheLineNo], newSize);
						bool reached_half_page = (my_ipt->free_bits < (512*8/2));
						pagemove = metacache->page_increase_on_line_increase(OSPAPFN, reached_half_page);
						my_ipt->line_offsets.byte[cacheLineNo] = newSize;	
						int old_free=my_ipt->free_bits;
						if((my_ipt->free_bits >= LSIZE) && (zinfo->is_line_movement_added) && (!pagemove)) {
								my_ipt->exceptions++;
								my_ipt->free_bits-=512;
								MemReq readReq, writeReq;
								readReq.set(MemReq::COMPRESSED_PAGE_MOVE);
								writeReq.set(MemReq::COMPRESSED_PAGE_MOVE);
								readReq.srcId = writeReq.srcId = req.srcId;
								readReq.cycle = writeReq.cycle = req.cycle;
								readReq.type = GETS; //First compression movement req
								readReq.lineAddr = (my_ipt->PA << zinfo->page_shift) | (cacheLineNo << lineBits);
								writeReq.lineAddr = (my_ipt->PA << zinfo->page_shift) | (cacheLineNo << lineBits);
								writeReq.cycle = zinfo->memoryControllers[0]->COMPaccess(readReq);
								writeReq.type = COMPRESS;
								zinfo->memoryControllers[0]->COMPaccess(writeReq);
								line_movement++;
								line_movement++;
						}
						if(old_free<LSIZE && (my_ipt->order != 8)) {
								my_ipt->exceptions++;
								MemReq readReq, writeReq;
								readReq.set(MemReq::COMPRESSED_PAGE_MOVE);
								writeReq.set(MemReq::COMPRESSED_PAGE_MOVE);
								readReq.srcId = writeReq.srcId = req.srcId;
								readReq.cycle = writeReq.cycle = req.cycle;
								readReq.type = GETS; //First compression movement req
								readReq.lineAddr = (my_ipt->PA << zinfo->page_shift) | (cacheLineNo << lineBits);
								writeReq.lineAddr = (my_ipt->PA << zinfo->page_shift) | (cacheLineNo << lineBits);
								writeReq.cycle = zinfo->memoryControllers[0]->COMPaccess(readReq);
								writeReq.type = COMPRESS;
								zinfo->memoryControllers[0]->COMPaccess(writeReq);
								line_movement++;
								line_movement++;
								Allo_Page[my_ipt->order]--;
								Address old_ppn = my_ipt->PA;
								if(my_ipt->order!=0){
									zinfo->buddy_allocator->free_one_page(my_ipt->PA, my_ipt->order-1);
									page = zinfo->buddy_allocator->allocate_pages(0, my_ipt->order);
									assert(page);
									my_ipt->PA = page->pageNo;
								}
								Allo_Page[my_ipt->order+1]++;
								my_ipt->free_bits+=(512*7);
								Address new_ppn = my_ipt->PA;
								info("Deallocated a page for increased size");
								line_movement++;
								line_movement++;

								//If we are moving a zero page to 512B, we still need to zero it out
								if (my_ipt->order==1){
									for(int lineNo = 0; lineNo < 8; lineNo++) {
										writeReq.lineAddr = (new_ppn << zinfo->page_shift) | (lineNo << lineBits);
										if(lineNo == _LINES_PER_PAGE-1){
											writeReq.type = COMPRESS;
										}
										zinfo->memoryControllers[0]->COMPaccess(writeReq);
									}
								}
								if (my_ipt->order==8){
										for(int lineNo = 0; lineNo < _LINES_PER_PAGE; lineNo++) {
												readReq.lineAddr = (old_ppn << zinfo->page_shift) | (lineNo << lineBits);
												writeReq.lineAddr = (new_ppn << zinfo->page_shift) | (lineNo << lineBits);
												writeReq.cycle = zinfo->memoryControllers[0]->COMPaccess(readReq);
												if(lineNo == _LINES_PER_PAGE-1){
														writeReq.type = COMPRESS;
												}
												zinfo->memoryControllers[0]->COMPaccess(writeReq);
										}
										pages_moved++;
								}
								pagemovecache->access(OSPAPFN);
								metacache->reset(OSPAPFN, 1);
						}
					}
				}
			}						
			futex_unlock(&walker_lock);
		}


		Address getOSPA(int core, Address lineAddr) {

			uint64_t shifter = ((uint64_t)core) << 50;
			lineAddr = lineAddr | shifter;
		//	info("searching VA %llx %llx", lineAddr, shifter);
			auto it = VAtoOSPA.find(lineAddr);
			if(it == VAtoOSPA.end())
				return -1;
			else { 
				//info("returning PA %llx %llx",  (*it).second, shifter);
				return (*it).second;
			}
		
		}

		Address do_dram_page_fault(MemReq& req, Address vpn ,uint32_t coreId, PAGE_FAULT fault_type , T* entry , bool is_itlb , bool &evict)
		{
			debug_printf("fetch pcm page %d to DRAM",(req.lineAddr>>zinfo->page_shift));
			//allocate dram buffer block
			DRAMBufferBlock* dram_block = allocate_page();
			if( dram_block)
			{
				Address dram_addr = block_id_to_addr( dram_block->block_id);
				//evict dram_block if it's dirty
				if( dram_block->is_occupied())
				{
					total_evict++;
					Address origin_ppn = dram_block->get_src_addr();
					if( dram_block->is_dirty())
					{
						evict = true;
						dirty_evict++;
						Address dst_addr = origin_ppn<<(zinfo->page_shift);
						//write back
						if( NVMainMemory::fetcher)
						{
							NVM::NVMainRequest* nvmain_req = new NVM::NVMainRequest();
							//from nvm to dram
							//is nvm address
							nvmain_req->address.SetPhysicalAddress(dram_addr,true);
							//buffer address
							nvmain_req->address.SetDestAddress(dst_addr, false);
							nvmain_req->burstCount = zinfo->block_size;
							nvmain_req->type = NVM::FETCH; 
							(NVMainMemory::fetcher)->IssueCommand( nvmain_req );
						}
						else 
						{
							req.cycle +=200;
						}
					}
					//remove relations between tlb and invalidate dram
					Address vaddr = dram_block->get_vaddr();	
					T* tlb_entry = NULL;
					HotMonitorTlb<T>* recover_tlb = NULL;
					//TLB shootdown1: shootdown evicted pages
					//TLB shootdown2: related to installed pages
					Address vpage_installed = entry->v_page_no;
					for( uint32_t i=0; i<zinfo->numCores; i++)
					{
						recover_tlb = dynamic_cast<HotMonitorTlb<T>* >
							(zinfo->cores[i]->getInsTlb());;
						tlb_entry = recover_tlb->look_up( vaddr );
						//instruction TLB shootdown(for PCM pages )
						//assume IPI is equal to TLB hit cycle
						if( tlb_entry)
						{
							reset_tlb( tlb_entry);
							hscc_tlb_shootdown += zinfo->tlb_hit_lat;
							req.cycle += zinfo->tlb_hit_lat;	//IPI latency 
							tlb_entry = NULL;
						}
						//instruction TLB shootdown(for DRAM pages)
						tlb_entry = recover_tlb->look_up(vpage_installed);
						if( tlb_entry)
						{
							reset_tlb(tlb_entry);
							hscc_tlb_shootdown += zinfo->tlb_hit_lat;
							req.cycle += zinfo->tlb_hit_lat;  //IPI latency
							tlb_entry = NULL;
						}
						//data TLB shootdown( for PCM pages)
						recover_tlb = dynamic_cast<HotMonitorTlb<T>* >
							(zinfo->cores[i]->getDataTlb());;
						tlb_entry = recover_tlb->look_up( vaddr );
						if( tlb_entry )
						{
							reset_tlb(tlb_entry);
							hscc_tlb_shootdown += zinfo->tlb_hit_lat;
							req.cycle += zinfo->tlb_hit_lat; //IPI latency
							tlb_entry = NULL;
						}
						//data TLB shootdown(for DRAM pages)
						tlb_entry = recover_tlb->look_up(vpage_installed);
						if( tlb_entry )
						{
							reset_tlb(tlb_entry);
							hscc_tlb_shootdown += zinfo->tlb_hit_lat;
							req.cycle += zinfo->tlb_hit_lat; //IPI latency
							tlb_entry = NULL;
						}
					}

					Page* page_ptr = zinfo->memory_node->get_page_ptr(origin_ppn); 
					Address overhead = paging->map_page_table((vaddr<<zinfo->page_shift),(void*)page_ptr,false);
					req.cycle += overhead;
					hscc_map_overhead += overhead;
					dram_block->invalidate();
				}
				//call memory controller interface to cache page
				if( NVMainMemory::fetcher)
				{
					NVM::NVMainRequest* nvm_req = new NVM::NVMainRequest();
					nvm_req->address.SetPhysicalAddress(req.lineAddr ,false );
					nvm_req->address.SetDestAddress(dram_addr , true);
					nvm_req->burstCount = zinfo->block_size;
					NVMainMemory::fetcher->IssueCommand( nvm_req );
				}
				else	//add fix latency express data buffering 
				{
					req.cycle += 200;
				}

				dram_block->validate(req.lineAddr>>(zinfo->block_shift));
				HotMonitorTlb<T>* tlb = NULL;		

				if( is_itlb )
					tlb = dynamic_cast<HotMonitorTlb<T>* >(zinfo->cores[coreId]->getInsTlb());
				else
					tlb = dynamic_cast<HotMonitorTlb<T>* >(zinfo->cores[coreId]->getDataTlb());

				tlb->remap_to_dram((dram_addr>>(zinfo->block_shift)) , entry);

				dram_block->set_src_addr( entry->p_page_no );
				dram_block->set_vaddr( entry->v_page_no);
				debug_printf("after remap , vpn:%llx , ppn:%llx",entry->v_page_no , entry->p_page_no);
				//update extended page table
				Address overhead= paging->map_page_table((vpn<<zinfo->page_shift),(void*)dram_block,true);
				req.cycle += overhead;
				hscc_map_overhead += overhead;	
				return dram_addr;
			}
			return INVALID_PAGE_ADDR;
		}
	public:
		PagingStyle mode;
		g_string pg_walker_name;
		BasePaging* paging;
		uint64_t period;
		unsigned long long tlb_shootdown_overhead;
		unsigned long long hscc_tlb_shootdown;
		unsigned long long pcm_map_overhead;
		unsigned long long hscc_map_overhead;

		unsigned long long tlb_miss_exclude_shootdown;
		unsigned long long tlb_miss_overhead;
		IPTable OSPA_to_PA_Table;
		std::map <Address, Address, std::less<Address>, Allocator<std::pair<Address, Address>>> VAtoOSPA;
		BPSCompressor64 *mycomp;
		uint64_t Allo_Page[9];
		uint64_t Init_Page[9];
		std::map<uint64_t, int, std::less<uint64_t>,  Allocator<std::pair<uint64_t, int>>> Moved_Pages;
		uint64_t metadata_accesses, metadata_misses;
		uint64_t pages_moved, line_movement;
		uint64_t compressed, total_pages;
	private:
		uint32_t procIdx;
		int mmap_cached;
		Address allocated_page;
		Address total_evict;
		Address dirty_evict;
		lock_t walker_lock;
		//TODO Make these pointers per core so that we dont have a problem checking OSPA
		FILE *f, *fflags;
		LRUFullycache *metacache;
		LRUFullycache *pagemovecache;

		//std::list<NVM::NVMainRequest*> failed_cache_request;
};
#else
template <class T>
class PageTableWalker: public BasePageTableWalker
{
	public:
		//access memory
		PageTableWalker(const g_string& name , PagingStyle style): pg_walker_name(name),procIdx((uint32_t)(-1))
	{
		mode = style;
		period = 0;
		dirty_evict = 0;
		total_evict = 0;
		allocated_page = 0;
		Allo_Page[4] = Allo_Page[3] = Allo_Page[2] = Allo_Page[1] = Allo_Page[0] = 0;
		Init_Page[4] = Init_Page[3] = Init_Page[2] = Init_Page[1] = Init_Page[0] = 0;
		metadata_accesses=metadata_misses=0;
		LRUFullycache* tempcache0 = gm_memalign<LRUFullycache>(CACHE_LINE_BYTES, 1); 
		LRUFullycache* tempcache1 = gm_memalign<LRUFullycache>(CACHE_LINE_BYTES, 1); 
		metacache = new (tempcache0) LRUFullycache(512); 
		pagemovecache = new (tempcache1) LRUFullycache(64); 
		pages_moved=line_movement=0;
		mmap_cached = 0;
		tlb_shootdown_overhead = 0;
		hscc_tlb_shootdown = 0;
		pcm_map_overhead = 0;
		hscc_map_overhead = 0;
		tlb_miss_exclude_shootdown = 0;
		tlb_miss_overhead = 0;
		futex_init(&walker_lock);
		BPSCompressor64* tempcomp =  gm_memalign<BPSCompressor64>(CACHE_LINE_BYTES, 1); 
		mycomp = new (tempcomp) BPSCompressor64("BPC64_5", 2, 4, 10, 2);   // ESHA GOLDEN // 64B, 8bins
		//mycomp = new BPSCompressor64("BPC64_5", 2, 4, 10, 2);   // ESHA GOLDEN // 64B, 8bins
		f = fopen("/proc/self/pagemap", "rb");
		fflags = fopen("/proc/kpageflags", "rb");
		info("zinfo->is_real_proce_based_paging : %d \n", zinfo->is_real_proce_based_paging);
		if(!f)
			info("ERROR :: Procfile pagemap could not be opened") ;
	}
		~PageTableWalker(){}
		/*------tlb hierarchy related-------*/
		//bool add_child(const char* child_name ,  BaseTlb* tlb);
		/*------simulation timing and state related----*/
		uint64_t access( MemReq& req)
		{

			assert(paging);
			period++;
			Address addr = PAGE_FAULT_SIG;
			Address init_cycle = req.cycle;
			if(!zinfo->is_real_proce_based_paging) {

				addr = paging->access(req);

				tlb_miss_exclude_shootdown += (req.cycle - init_cycle);
				//page fault
				if( addr == PAGE_FAULT_SIG )	
				{
					addr = do_page_fault(req , PCM_PAGE_FAULT);
				}
				tlb_miss_overhead += (req.cycle - init_cycle);
			}
			else {
				addr = lookup_procfile(req); //This is the VPN
				if(zinfo->is_main_mem_compressed) {
					Address FullOSPA = (addr << zinfo->page_shift) | (req.lineAddr & (zinfo->lineSize - 1));
					req.setOSPA(FullOSPA);
					//VA is the line addr in VA
					UINT64 pageAddr = (req.lineAddr >> zinfo->page_shift ) << zinfo->page_shift;

					//Now, we have the OSPA. Based on this, look up the <VA, PA> pair and match
					Address OSPAPFN;
					uint64_t shifter = ((uint64_t)req.srcId) << 50;
					int OSPAMatch = OSPA_to_PA_Table.OSPA_mapped(getOSPA(req.srcId,addr), shifter|pageAddr);
				//	info("TLB looked up : %llx found %d ", addr, OSPAMatch);
					if(OSPAMatch == 1) {
						//Nothing to do here, unless it is an LLC eviction -- 
						//then we will do it in mem
						//info("OSPA matched\n");
						req.cycle+=(zinfo->mem_access_time*2);
					}	
					else if (OSPAMatch == -1) {
						//ESHA : OSPA not found at all, allocate a page
						//Note that this is paging in the MPA space
						//info("OSPA not found\n");
						do_page_fault(req, PCM_PAGE_FAULT);
					}
					else if (OSPAMatch == -2) {
						//info("OSPA-VA not found\n");
						//DOING THIS HERE BECAUSE IN SIMULATOR, WE DONT SEE THE OS PAGE MOVES
						IPTEntry* PA_to_free = OSPA_to_PA_Table.getIPT(getOSPA(req.srcId,addr));
						OSPA_to_PA_Table.OSPA_erase(getOSPA(req.srcId, addr));
						if(PA_to_free->order == 0) {
							zinfo->buddy_allocator->free_one_page(PA_to_free->PA, PA_to_free->order);
						}
						else {
							zinfo->buddy_allocator->free_one_page(PA_to_free->PA, PA_to_free->order-1);
						}
						Allo_Page[PA_to_free->order]--;
						do_page_fault(req, PCM_PAGE_FAULT);
						//ESHA : Ask Mattan if this should be done or not
						//OSPA found, but VA not matched, free this page, 
						//allocate a new page of required size
					}
					else
						assert(0);
				}
				else{
					req.cycle+=(zinfo->mem_access_time*2);
				}
				tlb_miss_overhead += (req.cycle - init_cycle);
			}
			//suppose page table walking time when tlb miss is 20 cycles
			return addr;	//find address
		}

		void write_through( MemReq& req)
		{
			assert(paging);
			paging->access(req);
		}

		BasePaging* GetPaging()
		{ return paging;}
		void SetPaging( uint32_t proc_id , BasePaging* copied_paging)
		{
			futex_lock(&walker_lock);
			procIdx = proc_id;
			paging = copied_paging;
			futex_unlock(&walker_lock);
		}

		void convert_to_dirty( Address block_id)
		{
			zinfo->dram_manager->convert_to_dirty( procIdx , block_id );
		}
		const char* getName()
		{  return pg_walker_name.c_str(); }

		void calculate_stats()
		{
			//for(auto it=Moved_Pages.begin(); it!=Moved_Pages.end(); it++){
			//	info("%llx %d", it->first, it->second);
			//}
			info("%s evict time:%lu \t dirty evict time: %lu \n",getName(),total_evict,dirty_evict);
			info("%s allocated pages:%lu \n", getName(),allocated_page);
			info("%s TLB shootdown overhead:%llu \n", getName(), tlb_shootdown_overhead);
			info("%s HSCC TLB shootdown overhead:%llu \n", getName(), hscc_tlb_shootdown);
			info("%s PCM page mapping overhead:%llu \n", getName(), pcm_map_overhead);
			info("%s DRAM page mapping overhead:%llu \n", getName(), hscc_map_overhead);
			info("%s TLB miss overhead(exclude TLB shootdown and page fault): %llu", getName(),tlb_miss_exclude_shootdown);
			info("%s TLB miss overhead (include TLB shootdown and page fault): %llu",getName(), tlb_miss_overhead);
			info("%s METADATA Accesses: %llu   Misses: %llu",getName(), metadata_accesses, metadata_misses);
			info("%s Line Movement Read/Wr Requests: %llu ",getName(), line_movement);
			info("%s Pages Moved: %llu Page Movement Read/Wr Requests: %llu UniquePages: %d",getName(), pages_moved, pages_moved*128, Moved_Pages.size());

			total_pages = (Init_Page[0] + Init_Page[1] + Init_Page[2] + Init_Page[3] + Init_Page[4])*4096*8;
			compressed = (Init_Page[0])*(page_sizes[0]) + (Init_Page[1])*(page_sizes[1]) + (Init_Page[2])*(page_sizes[2]) + (Init_Page[3])*(page_sizes[3]) + (Init_Page[4])*(page_sizes[4]);
			info("In the beginning, pages are :  0: %lld 0.5K: %lld 1K: %lld 2K: %lld 4K: %lld", Init_Page[0] ,Init_Page[1] , Init_Page[2] , Init_Page[3], Init_Page[4]) ;
			info("Compression: %llu compressed: %llu %.6f", total_pages, compressed, (float)(total_pages)/(float)(compressed));

			total_pages = (Allo_Page[0] + Allo_Page[1] + Allo_Page[2] + Allo_Page[3] + Allo_Page[4])*4096*8;
			compressed = (Allo_Page[0])*(page_sizes[0]) + (Allo_Page[1])*(page_sizes[1]) + (Allo_Page[2])*(page_sizes[2]) + (Allo_Page[3])*(page_sizes[3]) + (Allo_Page[4])*(page_sizes[4]);
			info("In the end, pages are :  0: %lld 0.5K: %lld 1K: %lld 2K: %lld 4K: %lld", Allo_Page[0], Allo_Page[1], Allo_Page[2], Allo_Page[3], Allo_Page[4]) ;
			info("Compression: %llu compressed: %llu %.6f", total_pages, compressed, (float)(total_pages)/(float)(compressed));
			
			int order = 0;
			Allo_Page[4] = Allo_Page[3] = Allo_Page[2] = Allo_Page[1] = Allo_Page[0] = 0;
			for(auto it = VAtoOSPA.begin(); it!=VAtoOSPA.end(); it++){	
				uint64_t pageAddr = (it->first) << zinfo->page_shift;
				order = mycomp->find_page_size_needed(pageAddr, zinfo->cacheline_bins_type);
				Allo_Page[order]++;
			}
			total_pages = (Allo_Page[0] + Allo_Page[1] + Allo_Page[2] + Allo_Page[3] + Allo_Page[4])*4096*8;
			compressed = (Allo_Page[0])*(page_sizes[0]) + (Allo_Page[1])*(page_sizes[1]) + (Allo_Page[2])*(page_sizes[2]) + (Allo_Page[3])*(page_sizes[3]) + (Allo_Page[4])*(page_sizes[4]);
			info("MAX COMP RATIO END In the end, pages are :  0: %lld 0.5K: %lld 1K: %lld 2K: %lld 4K: %lld", Allo_Page[0], Allo_Page[1], Allo_Page[2], Allo_Page[3], Allo_Page[4]);
			info("Compression: %llu compressed: %llu %.6f", total_pages, compressed, (float)(total_pages)/(float)(compressed));

		}

		Address lookup_procfile(MemReq& req)
		{
			futex_lock(&walker_lock);
			//TLB shootdown
			Address vpn = req.lineAddr>>(zinfo->page_shift);
			BaseTlb* tmp_tlb = NULL;
			T* entry = NULL;
			for( uint64_t i = 0; i<zinfo->numCores; i++)
			{
				tmp_tlb = zinfo->cores[i]->getInsTlb();
				union
				{	
					CommonTlb<T>* com_tlb;
					HotMonitorTlb<T>* hot_tlb;
				};
				if( zinfo->tlb_type == COMMONTLB )
				{
					com_tlb = dynamic_cast<CommonTlb<T>* >(tmp_tlb); 
					entry = com_tlb->look_up(vpn);
				}
				//instruction TLB IPI
				if( entry )
				{
					entry->set_invalid();
					tlb_shootdown_overhead += zinfo-> tlb_hit_lat; 
					req.cycle += zinfo->tlb_hit_lat;
					entry = NULL;
				}
				tmp_tlb = zinfo->cores[i]->getDataTlb();
				if( zinfo->tlb_type == COMMONTLB)
				{
					com_tlb = dynamic_cast<CommonTlb<T>* >(tmp_tlb); 
					entry = com_tlb->look_up(vpn);
				}
				if(entry )
				{
					entry->set_invalid();
					tlb_shootdown_overhead += zinfo-> tlb_hit_lat; 
					req.cycle += zinfo->tlb_hit_lat;
					entry = NULL;
				}
			}
			//*********TLB shoot down ended
			int stall =0;
			uint64_t read_val = 0;
			uint64_t nread_val = 0;
			//info ("Src ID was %d", req.srcId);
			uint64_t shifter = ((uint64_t)req.srcId) << 50;
			if (zinfo->numCores == 1 ){	
				while(1) {
					uint64_t file_offset = vpn * PAGEMAP_ENTRY;
					int status = fseek(f, file_offset, SEEK_SET);
					if(status){
						info("ERROR :: Failed to do fseek! address: 0x%lx", vpn);
						//sleep(1);
						//return -1;

					}
					else {
						errno = 0;
						unsigned char c_buf[PAGEMAP_ENTRY];
						for(int i=0; i < PAGEMAP_ENTRY; i++){
							int c = getc(f);
							if(c==EOF){
								info("ERROR :: Reached end of the file\n");
								//VAtoOSPA[vpn] = (Address) (req.srcId << 20) | vpn;
								VAtoOSPA[ shifter|vpn] =  shifter|vpn; 
								futex_unlock(&walker_lock);
								return vpn;
							}
							if(is_bigendian())
								c_buf[i] = c;
							else
								c_buf[PAGEMAP_ENTRY - i - 1] = c;
						}
						for(int i=0; i < PAGEMAP_ENTRY; i++){
							read_val = (read_val << 8) + c_buf[i];
						}
						//printf("\n");
						if(GET_BIT(read_val, 63)){
							//info("PFN: 0x%llx vpn : 0x%lx",(unsigned long long) GET_PFN(read_val), vpn);
							//Now read the kpageflags
							uint64_t nfile_offset = ((unsigned long long)GET_PFN(read_val) )* PAGEMAP_ENTRY;
							int nstatus = fseek(fflags, nfile_offset, SEEK_SET);
							if(nstatus){
								info("ERROR :: Failed to do fseek! address: 0x%lx", vpn);
							}
							else {
								unsigned char nc_buf[PAGEMAP_ENTRY];
								for(int i=0; i < PAGEMAP_ENTRY; i++){
									int nc = getc(f);
									if(nc==EOF){
										info("ERROR :: Reached end of the file\n");
										//VAtoOSPA[vpn] = (req.srcId << 20) | vpn; 
										VAtoOSPA[ shifter |vpn] =  shifter |vpn; 
										//VAtoOSPA[vpn] = vpn;
										futex_unlock(&walker_lock);
										return vpn;
									}
									if(is_bigendian())
										nc_buf[i] = nc;
									else
										nc_buf[PAGEMAP_ENTRY - i - 1] = nc;
								}
								for(int i=0; i < PAGEMAP_ENTRY; i++){
									nread_val = (nread_val << 8) + nc_buf[i];
								}
								//							if(GET_BIT(nread_val, 22) || GET_BIT(nread_val, 17)){ //THP bit in kpageflags
								//								//info("THP");
								//							}
							}
							//ESHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA		
							//VAtoOSPA[vpn] = (Address) GET_PFN(read_val);
							//VAtoOSPA[vpn] = (req.srcId << 20) | vpn; 
							VAtoOSPA[ shifter|vpn] =  shifter |vpn; 
							futex_unlock(&walker_lock);
							return vpn;
						}
					}
					//ESHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAaa
					info("Stalling at 0x%lx", vpn);
					//if(stall++ == 1) break;			  
					//sleep(1);
					break;
				}
			}
			//VAtoOSPA[vpn] = (Address) vpn;
			VAtoOSPA[ shifter|vpn] =  shifter |vpn; 
			futex_unlock(&walker_lock);
			return shifter|vpn;
		}

		Address do_page_fault(MemReq& req, PAGE_FAULT fault_type)
		{
			//allocate one page from Zone_Normal area
			//ESHA If compression is ON, then the page's size is determined
			// based on the compressed size of the page
			futex_lock(&walker_lock);
			debug_printf("page fault, allocate free page through buddy allocator");
			UINT64 pageAddr = (req.lineAddr >> zinfo->page_shift ) << zinfo->page_shift;
			Page* page = NULL;
			unsigned order = 0;
			if( zinfo->is_main_mem_compressed ) {
				order = mycomp->find_page_size_needed(pageAddr, zinfo->cacheline_bins_type);
			}
			if( zinfo->buddy_allocator)
			{
				if(order == 0){
					page = zinfo->buddy_allocator->allocate_pages(0, order);
				}
				else{
					page = zinfo->buddy_allocator->allocate_pages(0, order-1);
				}
				Allo_Page[order]++;
				Init_Page[order]++;
				if(page)
				{
					//TLB shootdown
					Address vpn = req.lineAddr>>(zinfo->page_shift);
					BaseTlb* tmp_tlb = NULL;
					T* entry = NULL;
					for( uint64_t i = 0; i<zinfo->numCores; i++)
					{
						tmp_tlb = zinfo->cores[i]->getInsTlb();
						union
						{	
							CommonTlb<T>* com_tlb;
							HotMonitorTlb<T>* hot_tlb;
						};
						if( zinfo->tlb_type == COMMONTLB )
						{
							com_tlb = dynamic_cast<CommonTlb<T>* >(tmp_tlb); 
							entry = com_tlb->look_up(vpn);
						}
						else if( zinfo->tlb_type == HOTTLB)
						{
							hot_tlb = dynamic_cast<HotMonitorTlb<T>* >(tmp_tlb); 
							entry = hot_tlb->look_up(vpn);
						}
						//instruction TLB IPI
						if( entry )
						{
							entry->set_invalid();
							tlb_shootdown_overhead += zinfo-> tlb_hit_lat; 
							req.cycle += zinfo->tlb_hit_lat;
							entry = NULL;
						}
						tmp_tlb = zinfo->cores[i]->getDataTlb();
						if( zinfo->tlb_type == COMMONTLB)
						{
							com_tlb = dynamic_cast<CommonTlb<T>* >(tmp_tlb); 
							entry = com_tlb->look_up(vpn);
						}
						else if( zinfo->tlb_type == HOTTLB)
						{
							hot_tlb = dynamic_cast<HotMonitorTlb<T>* >(tmp_tlb); 
							entry = hot_tlb->look_up(vpn);
						}
						if(entry)
						{
							entry->set_invalid();
							tlb_shootdown_overhead += zinfo-> tlb_hit_lat; 
							req.cycle += zinfo->tlb_hit_lat;
							entry = NULL;
						}
					}
					//*********TLB shoot down ended
					if( zinfo->enable_shared_memory)
					{
						if( !map_shared_region(req, page) )
						{
							Address overhead = paging->map_page_table( req.lineAddr,(void*)page);
							pcm_map_overhead += overhead;
							req.cycle += overhead;
						}
					}
					else
					{
						Address overhead = paging->map_page_table( req.lineAddr,(void*)page);
						pcm_map_overhead += overhead;
						req.cycle += overhead;
					}
					allocated_page++;
					//cout << "Allocated page VA: " << hex << vpn << dec << " order " << order << endl ;
					CACHELINE_OFFSET_DATA lineOffsets = mycomp->find_cacheline_offsets(pageAddr, zinfo->cacheline_bins_type);
				
					uint64_t shifter = ((uint64_t)req.srcId) << 50;
					OSPA_to_PA_Table.insert(getOSPA(req.srcId,vpn), shifter|pageAddr, page->pageNo, lineOffsets, order, zinfo->cacheline_bins_type);	
					futex_unlock(&walker_lock);
					return getOSPA(req.srcId, req.lineAddr >> zinfo->page_shift);
				}
			}
			//update page table
			futex_unlock(&walker_lock);
			return (req.lineAddr>>zinfo->page_shift);
		}


		bool inline map_shared_region( MemReq& req , void* page)
		{
			Address vaddr = req.lineAddr;
			//std::cout<<"find out shared region"<<std::endl;
			if( !zinfo->shared_region[procIdx].empty())
			{
				int vm_size = zinfo->shared_region[procIdx].size();
				//std::cout<<"mmap_cached:"<<std::dec<<mmap_cached
				//	<<" vm size:"<<std::dec<<vm_size<<std::endl;
				Address vm_start = zinfo->shared_region[procIdx][mmap_cached].start;
				Address vm_end = zinfo->shared_region[procIdx][mmap_cached].end;
				Address vpn = vaddr>>(zinfo->page_shift);
				//belong to the shared memory region
				//examine whether in mmap_cached region (examine mmap_cached firstly)
				if( find_shared_vm(vaddr,
							zinfo->shared_region[procIdx][mmap_cached]) )
				{
					Address overhead = map_all_shared_memory( vaddr, (void*)page);
					req.cycle += overhead;
					pcm_map_overhead += overhead;
					return true;
				}
				//after mmap_cached
				else if( vpn > vm_end && mmap_cached < vm_size-1 )
				{
					mmap_cached++;
					for( ;mmap_cached < vm_size; mmap_cached++)
					{
						if( find_shared_vm(vaddr, 
									zinfo->shared_region[procIdx][mmap_cached]))
						{
							Address overhead = map_all_shared_memory(vaddr, (void*)page);
							req.cycle += overhead;
							pcm_map_overhead += overhead;
							return true;
						}
					}
					mmap_cached--;
				}
				//before mmap_cached
				else if( vpn < vm_start && mmap_cached > 0 )
				{
					mmap_cached--;
					for( ;mmap_cached >= 0; mmap_cached--)
					{
						if( find_shared_vm(vaddr, 
									zinfo->shared_region[procIdx][mmap_cached]))
						{
							Address overhead = map_all_shared_memory(vaddr, (void*)page);
							req.cycle += overhead;
							pcm_map_overhead += overhead;
							return true;
						}
					}
					mmap_cached++;
				}
			}
			return false;
		}

		bool inline find_shared_vm(Address vaddr, Section vm_sec)
		{
			Address vpn = vaddr >>(zinfo->page_shift);
			if( vpn >= vm_sec.start && vpn < vm_sec.end )
				return true;
			return false;
		}

		int inline map_all_shared_memory( Address va, void* page)
		{
			int latency = 0;
			for( uint32_t i=0; i<zinfo->numProcs; i++)
			{
				assert(zinfo->paging_array[i]);
				latency += zinfo->paging_array[i]->map_page_table(va,page);
			}
			return latency;
		}

		DRAMBufferBlock* allocate_page( )
		{
			if( zinfo->dram_manager->should_reclaim() )
			{
				zinfo->dram_manager->evict( zinfo->dram_evict_policy);
			}
			//std::cout<<"allocate page table"<<std::endl;
			return (zinfo->dram_manager)->allocate_one_page( procIdx);
		}

		void reset_tlb( T* tlb_entry)
		{
			tlb_entry->set_in_dram(false);
			tlb_entry->clear_counter();
		}

		bool update_metacache(MemReq& req) {
			uint64_t OSPAPFN = getOSPA(req.cid, req.lineAddr>>(zinfo->page_shift - lineBits));
			//info("Metadata : %lx " ,OSPAPFN );
			metadata_accesses++;
			if(metacache->access(OSPAPFN)) {
				return 1;
			} 
			return 0;
		}

		uint64_t metadata(MemReq& req) {	
			//The metadata prefetch is currently disabled
			//Seems to reduce the hit rate -- quite bad for non-spatially-local benches
			//metacache->access(OSPAPFN | 0x1); //One cache line has two entries
			//metacache->access(OSPAPFN & (-2)); //One cache line has two entries
			uint64_t OSPAPFN = getOSPA(req.cid, req.lineAddr>>(zinfo->page_shift - lineBits));
			IPTEntry *my_ipt = OSPA_to_PA_Table.getIPT(OSPAPFN);
			metadata_misses++;
			MemReq readReq;
			readReq.set(MemReq::COMPRESSED_PAGE_MOVE);
			readReq.srcId = req.srcId;
			readReq.cycle = req.cycle;
			//readReq.type = GETS;
			readReq.type = METADATA;
			readReq.lineAddr = (my_ipt->PA << 5); //Since each metadata entry is 32bytes
			return zinfo->memoryControllers[0]->COMPaccess(readReq);
		}

		void do_compress_size_check( MemReq& req) 
		{
			futex_lock(&walker_lock);
			//This re.lineAddr is OSPA line addr
			//Since this is not called before the l1 filter cache, lineAddr is cache line addr by this time
			//Now, we have the OSPA. Based on this, look up the <VA, PA> pair and match
			//info("Do_compress_Check");
			CACHELINE_DATA _data;
			uint64_t line_addr = 0;
			bool pagemove=0;
			uint64_t OSPAPFN = getOSPA(req.cid, req.lineAddr>>(zinfo->page_shift - lineBits));
			IPTEntry *my_ipt = OSPA_to_PA_Table.getIPT(OSPAPFN);
			PIN_SafeCopy(&_data, (VOID *) (req.lineAddr << lineBits), zinfo->lineSize); 

			unsigned newSize = linebin(mycomp->compressLine(&_data, line_addr), zinfo->cacheline_bins_type);
			unsigned cacheLineNo = (req.lineAddr & (_LINES_PER_PAGE-1)) ;

			Page* page = NULL;

			if ( newSize != my_ipt->line_offsets.byte[cacheLineNo]) {
				if(newSize < my_ipt->line_offsets.byte[cacheLineNo]) {
					//Size has reduced
					int increasingPage = metacache->line_decrease(OSPAPFN);
					int increasingPage2 = pagemovecache->line_decrease(OSPAPFN);
					info("Size reduced  0x%lx %d from %d to %d\n", req.lineAddr >> 6, my_ipt->VA, my_ipt->line_offsets.byte[cacheLineNo], newSize);
					my_ipt->free_bits += (block_sizes[zinfo->cacheline_bins_type][my_ipt->line_offsets.byte[cacheLineNo]] - block_sizes[zinfo->cacheline_bins_type][newSize]); 

					my_ipt->line_offsets.byte[cacheLineNo] = newSize;	
					if((my_ipt->order != 0) && (increasingPage == 0) && (increasingPage2==-1) && (my_ipt->free_bits >= (page_sizes[my_ipt->order] - page_sizes[my_ipt->order-1] ))) {
						MemReq readReq, writeReq;
						readReq.set(MemReq::COMPRESSED_PAGE_MOVE);
						writeReq.set(MemReq::COMPRESSED_PAGE_MOVE);
						readReq.srcId = writeReq.srcId = req.srcId;
						readReq.cycle = writeReq.cycle = req.cycle;
						readReq.type = GETS;
						writeReq.type = PUTX;

						Address old_ppn = my_ipt->PA;
						if(Moved_Pages.find(my_ipt->VA)!=Moved_Pages.end()){
							Moved_Pages[my_ipt->VA]++;
						}
						else {
							Moved_Pages[my_ipt->VA]=1;
						}
						info("Deallocating page 0x%lx %d for decreased size", my_ipt->VA, my_ipt->PA);
						if(my_ipt->order!=1){
							//All the -1 , -2 is because order = 0 corresponds to a zero page, but in the MPA, 
							//we still have an order 0 page allocated already
							zinfo->buddy_allocator->free_one_page(my_ipt->PA, my_ipt->order - 1);
							page = zinfo->buddy_allocator->allocate_pages(0, my_ipt->order - 2);
							assert(page);
							my_ipt->PA = page->pageNo;
						}
						Allo_Page[my_ipt->order]--;
						Allo_Page[my_ipt->order-1]++;
						Address new_ppn = my_ipt->PA;
						my_ipt->free_bits -= (page_sizes[my_ipt->order] - page_sizes[my_ipt->order-1]);
						my_ipt->order--;
						my_ipt->exceptions = 0;
						info("Deallocated a page for decreased size, 0x%lx to 0x%lx", old_ppn<<zinfo->page_shift, new_ppn<<zinfo->page_shift);
						pages_moved++;
						if(my_ipt->order!=0){
							for(int lineNo = 0; lineNo < _LINES_PER_PAGE; lineNo++) {
								readReq.lineAddr = (old_ppn << zinfo->page_shift) | (lineNo << lineBits);
								writeReq.lineAddr = (new_ppn << zinfo->page_shift) | (lineNo << lineBits);
								writeReq.cycle = zinfo->memoryControllers[0]->COMPaccess(readReq);
								if(lineNo == _LINES_PER_PAGE-1){
									writeReq.type = COMPRESS;
								}
								zinfo->memoryControllers[0]->COMPaccess(writeReq);
							}
						}
						metacache->reset(OSPAPFN, 0);
					}
				}
				else {
					if(my_ipt->order==4) {
						my_ipt->free_bits -= (block_sizes[zinfo->cacheline_bins_type][newSize] - block_sizes[zinfo->cacheline_bins_type][my_ipt->line_offsets.byte[cacheLineNo]]); 
						my_ipt->line_offsets.byte[cacheLineNo] = newSize;	
					}
					else {
						//Size has increased
						info("Size increased 0x%lx %lx %d from %d to %d\n", req.lineAddr >> 6, OSPAPFN, my_ipt->PA, my_ipt->line_offsets.byte[cacheLineNo], newSize);
						bool reached_half_page = (my_ipt->free_bits < (page_sizes[my_ipt->order]/2));
						pagemove = metacache->page_increase_on_line_increase(OSPAPFN, reached_half_page);
						my_ipt->line_offsets.byte[cacheLineNo] = newSize;	
						if((my_ipt->free_bits >= LSIZE) && (zinfo->is_line_movement_added) && (!pagemove)) {
							if(my_ipt->free_bits < LSIZE){
								my_ipt->free_bits -= (block_sizes[zinfo->cacheline_bins_type][newSize] - block_sizes[zinfo->cacheline_bins_type][my_ipt->line_offsets.byte[cacheLineNo]]); 
								MemReq readReq, writeReq;
								readReq.set(MemReq::COMPRESSED_PAGE_MOVE);
								writeReq.set(MemReq::COMPRESSED_PAGE_MOVE);
								readReq.srcId = writeReq.srcId = req.srcId;
								readReq.cycle = writeReq.cycle = req.cycle;
								readReq.type = GETS; //First compression movement req
								writeReq.type = PUTX;

								for(int lineNo = cacheLineNo; lineNo < _LINES_PER_PAGE; lineNo++) {
									readReq.lineAddr = (my_ipt->PA << zinfo->page_shift) | (lineNo << lineBits);
									writeReq.lineAddr = (my_ipt->PA << zinfo->page_shift) | (lineNo << lineBits);
									writeReq.cycle = zinfo->memoryControllers[0]->COMPaccess(readReq);
									if(lineNo == _LINES_PER_PAGE-1){
										writeReq.type = COMPRESS;
									}
									zinfo->memoryControllers[0]->COMPaccess(writeReq);
									line_movement++;
									line_movement++;
								}
							}
							else {
								my_ipt->exceptions++;
								my_ipt->free_bits-=512;
								MemReq readReq, writeReq;
								readReq.set(MemReq::COMPRESSED_PAGE_MOVE);
								writeReq.set(MemReq::COMPRESSED_PAGE_MOVE);
								readReq.srcId = writeReq.srcId = req.srcId;
								readReq.cycle = writeReq.cycle = req.cycle;
								readReq.type = GETS; //First compression movement req
								readReq.lineAddr = (my_ipt->PA << zinfo->page_shift) | (cacheLineNo << lineBits);
								writeReq.lineAddr = (my_ipt->PA << zinfo->page_shift) | (cacheLineNo << lineBits);
								writeReq.cycle = zinfo->memoryControllers[0]->COMPaccess(readReq);
								writeReq.type = COMPRESS;
								zinfo->memoryControllers[0]->COMPaccess(writeReq);
								line_movement++;
								line_movement++;
							}
						}
						if(pagemove || ((my_ipt->free_bits <= 0) && (my_ipt->order != 4))) {
							if(pagemove)
								info("Moving page due to prediction");
							MemReq readReq, writeReq;
							readReq.set(MemReq::COMPRESSED_PAGE_MOVE);
							writeReq.set(MemReq::COMPRESSED_PAGE_MOVE);
							readReq.srcId = writeReq.srcId = req.srcId;
							readReq.cycle = writeReq.cycle = req.cycle;
							readReq.type = GETS;
							writeReq.type = PUTX;

							Address old_ppn = my_ipt->PA;
							if(Moved_Pages.find(my_ipt->VA)!=Moved_Pages.end()){
								Moved_Pages[my_ipt->VA]++;
							}
							else {
								Moved_Pages[my_ipt->VA]=1;
							}
							info("Deallocating page 0x%lx %d for increased size", my_ipt->VA, my_ipt->PA);
							Allo_Page[my_ipt->order]--;
							if((metacache->get_inc_hist(OSPAPFN)) | (pagemovecache->line_decrease(OSPAPFN)!=-1) ){
								if(my_ipt->order==0)
									zinfo->buddy_allocator->free_one_page(my_ipt->PA, my_ipt->order);
								else
									zinfo->buddy_allocator->free_one_page(my_ipt->PA, my_ipt->order - 1);
								Allo_Page[4]++;
								page = zinfo->buddy_allocator->allocate_pages(0, 3);
								assert(page);
								my_ipt->PA = page->pageNo;
								my_ipt->free_bits += (page_sizes[4] - page_sizes[my_ipt->order]);
								my_ipt->order=4;
							}
							else {
								if(my_ipt->order!=0){
									zinfo->buddy_allocator->free_one_page(my_ipt->PA, my_ipt->order-1);
									page = zinfo->buddy_allocator->allocate_pages(0, my_ipt->order);
									assert(page);
									my_ipt->PA = page->pageNo;
								}
								Allo_Page[my_ipt->order+1]++;
								my_ipt->free_bits += (page_sizes[my_ipt->order+1] - page_sizes[my_ipt->order]);
								my_ipt->order++;
							}
							my_ipt->exceptions = 0;
							Address new_ppn = my_ipt->PA;
							info("Deallocated a page for increased size");
							pages_moved++;
							//If we are moving a zero page to 512B, we still need to zero it out
							for(int lineNo = 0; lineNo < _LINES_PER_PAGE; lineNo++) {
								readReq.lineAddr = (old_ppn << zinfo->page_shift) | (lineNo << lineBits);
								writeReq.lineAddr = (new_ppn << zinfo->page_shift) | (lineNo << lineBits);
								writeReq.cycle = zinfo->memoryControllers[0]->COMPaccess(readReq);
								if(lineNo == _LINES_PER_PAGE-1){
									writeReq.type = COMPRESS;
								}
								zinfo->memoryControllers[0]->COMPaccess(writeReq);
							}
							pagemovecache->access(OSPAPFN);
							metacache->reset(OSPAPFN, 1);
						}
					}
				}
			}						
			futex_unlock(&walker_lock);
		}


		Address getOSPA(int core, Address lineAddr) {

			uint64_t shifter = ((uint64_t)core) << 50;
			lineAddr = lineAddr | shifter;
		//	info("searching VA %llx %llx", lineAddr, shifter);
			auto it = VAtoOSPA.find(lineAddr);
			if(it == VAtoOSPA.end())
				return -1;
			else { 
				//info("returning PA %llx %llx",  (*it).second, shifter);
				return (*it).second;
			}
		
		}

		Address do_dram_page_fault(MemReq& req, Address vpn ,uint32_t coreId, PAGE_FAULT fault_type , T* entry , bool is_itlb , bool &evict)
		{
			debug_printf("fetch pcm page %d to DRAM",(req.lineAddr>>zinfo->page_shift));
			//allocate dram buffer block
			DRAMBufferBlock* dram_block = allocate_page();
			if( dram_block)
			{
				Address dram_addr = block_id_to_addr( dram_block->block_id);
				//evict dram_block if it's dirty
				if( dram_block->is_occupied())
				{
					total_evict++;
					Address origin_ppn = dram_block->get_src_addr();
					if( dram_block->is_dirty())
					{
						evict = true;
						dirty_evict++;
						Address dst_addr = origin_ppn<<(zinfo->page_shift);
						//write back
						if( NVMainMemory::fetcher)
						{
							NVM::NVMainRequest* nvmain_req = new NVM::NVMainRequest();
							//from nvm to dram
							//is nvm address
							nvmain_req->address.SetPhysicalAddress(dram_addr,true);
							//buffer address
							nvmain_req->address.SetDestAddress(dst_addr, false);
							nvmain_req->burstCount = zinfo->block_size;
							nvmain_req->type = NVM::FETCH; 
							(NVMainMemory::fetcher)->IssueCommand( nvmain_req );
						}
						else 
						{
							req.cycle +=200;
						}
					}
					//remove relations between tlb and invalidate dram
					Address vaddr = dram_block->get_vaddr();	
					T* tlb_entry = NULL;
					HotMonitorTlb<T>* recover_tlb = NULL;
					//TLB shootdown1: shootdown evicted pages
					//TLB shootdown2: related to installed pages
					Address vpage_installed = entry->v_page_no;
					for( uint32_t i=0; i<zinfo->numCores; i++)
					{
						recover_tlb = dynamic_cast<HotMonitorTlb<T>* >
							(zinfo->cores[i]->getInsTlb());;
						tlb_entry = recover_tlb->look_up( vaddr );
						//instruction TLB shootdown(for PCM pages )
						//assume IPI is equal to TLB hit cycle
						if( tlb_entry)
						{
							reset_tlb( tlb_entry);
							hscc_tlb_shootdown += zinfo->tlb_hit_lat;
							req.cycle += zinfo->tlb_hit_lat;	//IPI latency 
							tlb_entry = NULL;
						}
						//instruction TLB shootdown(for DRAM pages)
						tlb_entry = recover_tlb->look_up(vpage_installed);
						if( tlb_entry)
						{
							reset_tlb(tlb_entry);
							hscc_tlb_shootdown += zinfo->tlb_hit_lat;
							req.cycle += zinfo->tlb_hit_lat;  //IPI latency
							tlb_entry = NULL;
						}
						//data TLB shootdown( for PCM pages)
						recover_tlb = dynamic_cast<HotMonitorTlb<T>* >
							(zinfo->cores[i]->getDataTlb());;
						tlb_entry = recover_tlb->look_up( vaddr );
						if( tlb_entry )
						{
							reset_tlb(tlb_entry);
							hscc_tlb_shootdown += zinfo->tlb_hit_lat;
							req.cycle += zinfo->tlb_hit_lat; //IPI latency
							tlb_entry = NULL;
						}
						//data TLB shootdown(for DRAM pages)
						tlb_entry = recover_tlb->look_up(vpage_installed);
						if( tlb_entry )
						{
							reset_tlb(tlb_entry);
							hscc_tlb_shootdown += zinfo->tlb_hit_lat;
							req.cycle += zinfo->tlb_hit_lat; //IPI latency
							tlb_entry = NULL;
						}
					}

					Page* page_ptr = zinfo->memory_node->get_page_ptr(origin_ppn); 
					Address overhead = paging->map_page_table((vaddr<<zinfo->page_shift),(void*)page_ptr,false);
					req.cycle += overhead;
					hscc_map_overhead += overhead;
					dram_block->invalidate();
				}
				//call memory controller interface to cache page
				if( NVMainMemory::fetcher)
				{
					NVM::NVMainRequest* nvm_req = new NVM::NVMainRequest();
					nvm_req->address.SetPhysicalAddress(req.lineAddr ,false );
					nvm_req->address.SetDestAddress(dram_addr , true);
					nvm_req->burstCount = zinfo->block_size;
					NVMainMemory::fetcher->IssueCommand( nvm_req );
				}
				else	//add fix latency express data buffering 
				{
					req.cycle += 200;
				}

				dram_block->validate(req.lineAddr>>(zinfo->block_shift));
				HotMonitorTlb<T>* tlb = NULL;		

				if( is_itlb )
					tlb = dynamic_cast<HotMonitorTlb<T>* >(zinfo->cores[coreId]->getInsTlb());
				else
					tlb = dynamic_cast<HotMonitorTlb<T>* >(zinfo->cores[coreId]->getDataTlb());

				tlb->remap_to_dram((dram_addr>>(zinfo->block_shift)) , entry);

				dram_block->set_src_addr( entry->p_page_no );
				dram_block->set_vaddr( entry->v_page_no);
				debug_printf("after remap , vpn:%llx , ppn:%llx",entry->v_page_no , entry->p_page_no);
				//update extended page table
				Address overhead= paging->map_page_table((vpn<<zinfo->page_shift),(void*)dram_block,true);
				req.cycle += overhead;
				hscc_map_overhead += overhead;	
				return dram_addr;
			}
			return INVALID_PAGE_ADDR;
		}
	public:
		PagingStyle mode;
		g_string pg_walker_name;
		BasePaging* paging;
		uint64_t period;
		unsigned long long tlb_shootdown_overhead;
		unsigned long long hscc_tlb_shootdown;
		unsigned long long pcm_map_overhead;
		unsigned long long hscc_map_overhead;

		unsigned long long tlb_miss_exclude_shootdown;
		unsigned long long tlb_miss_overhead;
		IPTable OSPA_to_PA_Table;
		std::map <Address, Address, std::less<Address>, Allocator<std::pair<Address, Address>>> VAtoOSPA;
		BPSCompressor64 *mycomp;
		uint64_t Allo_Page[5];
		uint64_t Init_Page[5];
		std::map<uint64_t, int, std::less<uint64_t>,  Allocator<std::pair<uint64_t, int>>> Moved_Pages;
		uint64_t metadata_accesses, metadata_misses;
		uint64_t pages_moved, line_movement;
		uint64_t compressed, total_pages;
	private:
		uint32_t procIdx;
		int mmap_cached;
		Address allocated_page;
		Address total_evict;
		Address dirty_evict;
		lock_t walker_lock;
		//TODO Make these pointers per core so that we dont have a problem checking OSPA
		FILE *f, *fflags;
		LRUFullycache *metacache;
		LRUFullycache *pagemovecache;

		//std::list<NVM::NVMainRequest*> failed_cache_request;
};
#endif
#endif

