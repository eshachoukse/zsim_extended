
/*
 * 2016-xxx by Esha Choukse
 * function: Inverted page table for mapping the OSPA to <VA,PA>
 * Also, it needs to track the compression ratio per line here
 */
#ifndef IPT_H
#define IPT_H

#include <map>
#include <fstream>
#include <iostream>
#include <utility>
#include "common_compressor.hh"
#include "log.h"

#define _TLB_LINEINFO_BITS_PER_DATA 3

typedef struct IPTEntry {
	uint64_t VA;
	uint64_t PA;
	CACHELINE_OFFSET_DATA line_offsets; 
	int free_bits;
	int order;
	int predictor;
	int exceptions;
} IPTEntry;

class IPTable
{
	private:
		std::map <uint64_t, IPTEntry, std::less<uint64_t>,  Allocator<std::pair<uint64_t,IPTEntry>>> myIPT;
	public:
		IPTable(){}
		void insert(uint64_t OSPA, uint64_t VA, uint64_t PA, CACHELINE_OFFSET_DATA lineOff, int order, int cacheline_bins_type) {
			IPTEntry newentry;
			newentry.VA = VA;
			newentry.PA = PA;
			newentry.line_offsets = lineOff;
			newentry.free_bits = leftoverSize(lineOff, cacheline_bins_type);
			newentry.order = order;
			newentry.predictor = 0;
			newentry.exceptions = 0;
			myIPT.insert(std::make_pair(OSPA, newentry));
			info("IPTable inserted entry VA: %lx PA: %lx order %d ", VA, PA, order);
		} 
		
		// Match the OSPA and VA -- if they dont match, return -1. Else, return the PA
		// If -1 was returned because of 
		//	No entry for the OSPA, get a new page from the buddy allocator
		//	a mismatch in VA, remove the existing page in buddy allocator and get a new one

		int OSPA_mapped( uint64_t OSPA, uint64_t VA) {
			auto it = myIPT.find(OSPA);
			if (it == myIPT.end())
				return -1;
			else if ((*it).second.VA != VA) {
				return -2;
			}
			else return 1;	
		}
		void OSPA_erase( uint64_t OSPA) {
			myIPT.erase(OSPA);
		}

		uint64_t getPA (uint64_t OSPA) {
			auto it = myIPT.find(OSPA);
			return (*it).second.PA;
		}

		int getLineSize (uint64_t OSPA, int lineNo) {
			auto it = myIPT.find(OSPA);
			if (it == myIPT.end())
				return -1;
			else {	
				return (*it).second.line_offsets.byte[lineNo];
			}
		}

		IPTEntry* getIPT (uint64_t OSPA) {
			auto it = myIPT.find(OSPA);
			if (it == myIPT.end())
				return NULL;
			else {	
				return &((*it).second);
			}
		}

};


#endif
