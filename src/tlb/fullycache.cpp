#include "tlb/page_table_walker.h"
#include "tlb/fullycache.h"
void LRUFullycache::update_ipt( uint64_t address) {
	PageTableWalker<TlbEntry>* recoverPT;
	recoverPT = (dynamic_cast<PageTableWalker<TlbEntry>* >(zinfo->pg_walkers[0]));
	IPTEntry *myIPT = (recoverPT->OSPA_to_PA_Table.getIPT(address));
	myIPT->predictor=address_to_prediction[address];
	//info("update ipt %lx %d ", address, address_to_prediction[address]);
}
int LRUFullycache::get_predictor(uint64_t address){
	PageTableWalker<TlbEntry>* recoverPT;
	recoverPT = (dynamic_cast<PageTableWalker<TlbEntry>* >(zinfo->pg_walkers[0]));
	IPTEntry *myIPT = (recoverPT->OSPA_to_PA_Table.getIPT(address));
	//info("get ipt %lx %d ", address, myIPT->predictor);
	return myIPT->predictor;
}
