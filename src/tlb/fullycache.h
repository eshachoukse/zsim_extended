#ifndef LRUFULLY_H
#define LRUFULLY_H
#include<iostream>
#include<map>
#include<stdlib.h>
#include<stdint.h>

using namespace std;
//Generic fully associative LRU cache

#include "zsim.h"
#include "tlb/tlb_entry.h"
#include "tlb/ipt.h"
class LRUFullycache
{
	public:
		LRUFullycache(int entries) {
			n_entries = entries;
			hits=0;
			misses=0;
			curcycle=0;
		}
		bool access(uint64_t address) {
			curcycle++;
			if (address_to_lru.find(address)!=address_to_lru.end()) {
				hits++;
				auto it = lru_to_address.find(address_to_lru[address]);
//ESHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
				//lru_to_address.erase(it);
				lru_to_address.erase(address_to_lru[address]);
				address_to_lru[address] = curcycle;
				lru_to_address[curcycle] = address;
				return 1;
			}
			else {
				misses++;
			
				if(address_to_lru.size() >= n_entries) {
					auto it = lru_to_address.begin();
					//address_to_lru.erase(address_to_lru.find(it->second));
					address_to_lru.erase(it->second);
					//lru_to_address.erase(it);
					lru_to_address.erase(it->first);
					address_to_lru[address]=curcycle;
					address_to_prediction[address]=get_predictor(address);
					address_to_hist[address]=0;
					lru_to_address[curcycle]=address;	
				}
				else {
					address_to_lru[address]=curcycle;
					address_to_prediction[address]=get_predictor(address);
					address_to_hist[address]=0;
					lru_to_address[curcycle]=address;	
				}
				return 0;
			}
		}
		void reset(uint64_t address, bool increased) {
		//Called upon page size change -- metadata is changed
			if(address_to_prediction[address]==3)
				address_to_prediction[address]=4;
			else
				address_to_prediction[address]=0;
			update_ipt(address);
			if(increased)
				address_to_hist[address]|=0x1;
			else
				address_to_hist[address]|=0x2;
		}

		int line_decrease(uint64_t address) {
			if (address_to_lru.find(address)==address_to_lru.end()) 
				return -1;
			if(address_to_prediction[address]>0) {
				address_to_prediction[address]--;
				update_ipt(address);
			}
			if(address_to_hist[address] & 0x1 == 1)
				return 1; // If the page was increased earlier, dont let it reduce
			else
				return address_to_prediction[address];
		}

		bool page_increase_on_line_increase(uint64_t address, bool reached_half_page) {
			if(address_to_lru.find(address)==address_to_lru.end())
				return 0;
			int pred = address_to_prediction[address] & 0x3;
			int last_increased_from_pred = address_to_prediction[address] & 0x4; 
			if(reached_half_page) {
				address_to_prediction[address] = address_to_prediction[address] & 0xb;
				update_ipt(address);	
				info("reset the higher part prediction");
			}
			if(last_increased_from_pred) 
				return 0;
			if(address_to_prediction[address] == 3)
				return 1;
			bool returnVal = (address_to_prediction[address] == 2)?1:0;
			address_to_prediction[address]++;
			update_ipt(address);
			return returnVal;
		}
		bool get_inc_hist(uint64_t address){
			return (address_to_hist[address] & 0x1);
		}
		void update_ipt( uint64_t address);
		int get_predictor(uint64_t address);
		int get_hits() { return hits; }
		int get_misses() { return misses;}
	private:
		int hits , misses, n_entries;
		uint64_t curcycle;
		//// In the predictor, it is 2 bit, 0 means do nothing, 1 means increase the page size
		g_map<uint64_t, uint64_t>  address_to_lru;
		g_map<uint64_t, uint64_t>  address_to_prediction;
		g_map<uint64_t, int>  address_to_hist;
		g_map<uint64_t, uint64_t>  lru_to_address;
		//std::map<uint64_t, uint64_t, std::less<uint64_t>,  Allocator<std::pair<uint64_t, uint64_t>>>  address_to_lru;
		//// In the predictor, it is 2 bit, 0 means do nothing, 1 means increase the page size
		//std::map<uint64_t, uint64_t, std::less<uint64_t>,  Allocator<std::pair<uint64_t, uint64_t>>>  address_to_prediction;
		//std::map<uint64_t, int, std::less<uint64_t>,  Allocator<std::pair<uint64_t, int>>>  address_to_hist;
		//std::map<uint64_t, uint64_t, std::less<uint64_t>,  Allocator<std::pair<uint64_t, uint64_t>>>  lru_to_address;
//Add a 2 bit predictor per page -- add a function that is called upon size increase
};
#endif
